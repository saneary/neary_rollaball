﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Playercontroller : MonoBehaviour {

    public float speed; // value for movement speed

    public float jumpPower; // value for jump power

    bool onGround;

    public Text countText; // reference for the counttext

    public Text winText; // reference for the wintext

   

    private Rigidbody rb;

    private int count;

    void Start  ()
    {
        
        rb = GetComponent<Rigidbody>();
        count = 0; // sets count to 0 at start
        SetCountText ();
        winText.text = ""; // sets no win text at start
    }
    void Jump()
    {
        rb.AddForce(Vector3.up * jumpPower);
    }
    private void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, .55f);
        if (Input.GetKeyDown(KeyCode.Space)) //sets the space bar as the control to jump
        {
            Jump();
        }
        
    }
    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis("Horizontal"); //inputs for movement
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed); // function to get movement speed
        
    }
    void OnTriggerEnter(Collider other) //functions for collisions
    {
        if (other.gameObject.CompareTag("Pick Up")) // If the player hits a pickup...
        {
            other.gameObject.SetActive (false); // the pickup disappears..
            count = count + 1; // and the count goes up accordingly
            SetCountText ();
        }
        if (other.gameObject.CompareTag("Floor")) // If the player falls off...
        {
            winText.text = "You Lose!"; // then the game displays the you lose text
        }
    }
    void SetCountText () //function for our counttext
    {
        countText.text = "Count: " + count.ToString (); // sets text format
        if (count >= 8) // sets winning score at 8
        {
            winText.text = "You Win!"; // text if you win
        }
    }
}
